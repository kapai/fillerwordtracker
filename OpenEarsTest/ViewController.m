//
//  ViewController.m
//  OpenEarsTest
//
//  Created by  on 10/8/15.
//  Copyright © 2015 Anuradha Pai. All rights reserved.
//

#import "ViewController.h"
#import "MyFillerWordsTVC.h"

#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEPocketsphinxController.h>

//#import <RapidEarsDemo/OEPocketsphinxController+RapidEars.h>
//#import <RapidEarsDemo/OEEventsObserver+RapidEars.h>


#import <OpenEars/OEAcousticModel.h>
#import <OpenEars/OEEventsObserver.h>


@interface ViewController () <OEEventsObserverDelegate, OptionsDelegate>
@property (strong, nonatomic) OEEventsObserver *openEarsEventsObserver;
@property (weak, nonatomic) IBOutlet UILabel *speechTextField;
//@property (nonatomic) BOOL hasMicrophonePermissions;
@property (weak, nonatomic) IBOutlet UIButton *startStopListeningButton;

@property(nonatomic, strong) NSMutableArray *fillerWordsArray;
@property(nonatomic, strong) NSMutableDictionary *fillerWordsTracked;


@end

@implementation ViewController

- (void)viewDidAppear:(BOOL)animated{
    [[OEPocketsphinxController sharedInstance] requestMicPermission];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.openEarsEventsObserver = [[OEEventsObserver alloc] init];
    [self.openEarsEventsObserver setDelegate:self];
    
    _fillerWordsTracked = [[NSMutableDictionary alloc] initWithCapacity:7];
    if (self.fillerWordsArray == nil) {
        self.fillerWordsArray = [[NSMutableArray alloc]init];
        NSArray *words = [[NSArray alloc] initWithObjects:@"NICE", @"AH", @"LIKE", @"SO", @"ACTUALLY", @"YOU KNOW", @"ONLY", nil];
        
        [self.fillerWordsArray addObjectsFromArray:words];
        NSLog(@"--- words added into array  --- %@", self.fillerWordsArray);
    }
}


- (IBAction)didClickonStartStopListeningButton:(UIButton *)sender {
    
    if ([[sender titleLabel].text isEqualToString:@"Start Listening"]) {
//        [[OEPocketsphinxController sharedInstance] requestMicPermission];
        [self activateOpenEarsAndStartListening];
        [sender setTitle:@"Stop Listening" forState:UIControlStateNormal];
        _speechTextField.hidden = NO;
        [_fillerWordsTracked removeAllObjects];
        
    }else{
        [[OEPocketsphinxController sharedInstance] stopListening];
        [sender setTitle:@"Start Listening" forState:UIControlStateNormal];
        _speechTextField.hidden = NO;
    }
    
}

#pragma mark OptionsDelegate methods
// Options delegate method implementation
-(void)optionsSendingFillerWordsArray:(NSMutableArray *)fillerWordsArray
{
    _fillerWordsArray = fillerWordsArray;
}



#pragma mark OEEventsObserverDelegate methods

- (void) pocketsphinxDidReceiveHypothesis:(NSString *)hypothesis recognitionScore:(NSString *)recognitionScore utteranceID:(NSString *)utteranceID {
    NSLog(@"The received hypothesis is %@ with a score of %@ and an ID of %@", hypothesis, recognitionScore, utteranceID);
        _speechTextField.text = hypothesis;
    
    //break hypothesis to filler words
    NSArray *array = [hypothesis componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    array = [array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]];
    
    for(NSString *fillerWord in array){
        NSNumber *fillerWordCount = _fillerWordsTracked[fillerWord];
        if (fillerWordCount) {
            NSNumber *value = [NSNumber numberWithInt:[fillerWordCount intValue] + 1];
            _fillerWordsTracked[fillerWord] = value;
        }else{
            _fillerWordsTracked[fillerWord] = [NSNumber numberWithInt:1];
        }

    }
}

- (void) pocketsphinxDidStartListening {
    NSLog(@"Pocketsphinx is now listening.");
    _speechTextField.text = @"Listening...";
}

- (void) pocketsphinxDidDetectSpeech {
    NSLog(@"Pocketsphinx has detected speech.");
    _speechTextField.text = @"Detecting Speech...";
}

- (void) pocketsphinxDidDetectFinishedSpeech {
    NSLog(@"Pocketsphinx has detected a period of silence, concluding an utterance.");
    _speechTextField.text = @"Detecting a period of silence, concluding an utterance....";
}

- (void) pocketsphinxDidStopListening {
    NSLog(@"Pocketsphinx has stopped listening.");
    NSMutableString *fillerWordsReportString = [[NSMutableString alloc] init];
    
    if ([[_fillerWordsTracked allKeys] count]> 0) {
        for (NSString *key in _fillerWordsTracked) {
            NSString *string = [NSString stringWithFormat:@"%@ : %d \n",key, [(NSNumber*)_fillerWordsTracked[key] intValue]];
            [fillerWordsReportString appendString:string];
        }
    }else
        [fillerWordsReportString appendString:@"No Filler Words Tracked"];

    _speechTextField.text = fillerWordsReportString;

}

- (void) pocketsphinxDidSuspendRecognition {
    NSLog(@"Pocketsphinx has suspended recognition.");
}

- (void) pocketsphinxDidResumeRecognition {
    NSLog(@"Pocketsphinx has resumed recognition.");
}

- (void) pocketsphinxDidChangeLanguageModelToFile:(NSString *)newLanguageModelPathAsString andDictionary:(NSString *)newDictionaryPathAsString {
    NSLog(@"Pocketsphinx is now using the following language model: \n%@ and the following dictionary: %@",newLanguageModelPathAsString,newDictionaryPathAsString);
}

- (void) pocketSphinxContinuousSetupDidFailWithReason:(NSString *)reasonForFailure {
    NSLog(@"Listening setup wasn't successful and returned the failure reason: %@", reasonForFailure);
}

- (void) pocketSphinxContinuousTeardownDidFailWithReason:(NSString *)reasonForFailure {
    NSLog(@"Listening teardown wasn't successful and returned the failure reason: %@", reasonForFailure);
}

- (void) testRecognitionCompleted {
    NSLog(@"A test file that was submitted for recognition is now complete.");
}



- (void)activateOpenEarsAndStartListening{
    
    OELanguageModelGenerator *lmGenerator = [[OELanguageModelGenerator alloc] init];

    NSString *name = @"NameIWantForMyLanguageModelFiles";
    NSError *err = [lmGenerator generateLanguageModelFromArray:self.fillerWordsArray withFilesNamed:name forAcousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]]; // Change "AcousticModelEnglish" to "AcousticModelSpanish" to create a Spanish language model instead of an English one.
    
    NSString *lmPath = nil;
    NSString *dicPath = nil;
    
    if(err == nil) {
        
        lmPath = [lmGenerator pathToSuccessfullyGeneratedLanguageModelWithRequestedName:@"NameIWantForMyLanguageModelFiles"];
        dicPath = [lmGenerator pathToSuccessfullyGeneratedDictionaryWithRequestedName:@"NameIWantForMyLanguageModelFiles"];
        
    } else {
        NSLog(@"Error: %@",[err localizedDescription]);
    }

    
    [[OEPocketsphinxController sharedInstance] setActive:TRUE error:nil];
    [[OEPocketsphinxController sharedInstance] startListeningWithLanguageModelAtPath:lmPath dictionaryAtPath:dicPath acousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"] languageModelIsJSGF:NO]; // Change "AcousticModelEnglish" to "AcousticModelSpanish" to perform Spanish recognition instead of English.
    
    
    
    //    [[OEPocketsphinxController sharedInstance] startRealtimeListeningWithLanguageModelAtPath:lmPath dictionaryAtPath:dicPath acousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]]; // Starts the rapid recognition loop. Change "AcousticModelEnglish" to "AcousticModelSpanish" in order to perform Spanish language recognition.
    
    
}

- (void) micPermissionCheckCompleted:(BOOL)result{
    if (!result) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Permission Required" message:@"This application requires the microphone to record speech. Please allow microphone access from Settings -> Privacy -> Microphone and give access to the application" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSLog(@"------request for permissions --------");
            
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        _startStopListeningButton.enabled = YES;
    }
    
    NSLog (@"mic permissions received");
    
}

-(void) pocketsphinxFailedNoMicPermissions{
    NSLog(@"mic permissions failed");
}


//Rapid ears delegate methods
#pragma mark RapidearsDelegate methods
//- (void) rapidEarsDidReceiveLiveSpeechHypothesis:(NSString *)hypothesis recognitionScore:(NSString *)recognitionScore {
//    NSLog(@"rapidEarsDidReceiveLiveSpeechHypothesis: %@",hypothesis);
//}
//
//- (void) rapidEarsDidReceiveFinishedSpeechHypothesis:(NSString *)hypothesis recognitionScore:(NSString *)recognitionScore {
//    NSLog(@"rapidEarsDidReceiveFinishedSpeechHypothesis: %@",hypothesis);
//}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.destinationViewController isKindOfClass:[MyFillerWordsTVC class]]) {
        
        if ([[self.startStopListeningButton titleLabel].text isEqualToString:@"Stop Listening"]) {
            [[OEPocketsphinxController sharedInstance] stopListening];
            [self.startStopListeningButton setTitle:@"Start Listening" forState:UIControlStateNormal];
            _speechTextField.hidden = YES;
        }
        MyFillerWordsTVC *optionsVC = [segue destinationViewController];
        optionsVC.optionsDelegateObject = self;
        optionsVC.fillerwords = self.fillerWordsArray;
    }
}

@end
