//
//  AppDelegate.h
//  OpenEarsTest
//
//  Created by  on 10/8/15.
//  Copyright © 2015 Anuradha Pai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

