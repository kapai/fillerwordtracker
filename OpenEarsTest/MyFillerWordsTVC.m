//
//  MyFillerWordsTVC.m
//  FillerWordTracker
//
//  Created by  on 11/8/15.
//  Copyright © 2015 Anuradha Pai. All rights reserved.
//

#import "MyFillerWordsTVC.h"

@interface MyFillerWordsTVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *word1;
@property (weak, nonatomic) IBOutlet UITextField *word2;
@property (weak, nonatomic) IBOutlet UITextField *word3;
@property (weak, nonatomic) IBOutlet UITextField *word4;
@property (weak, nonatomic) IBOutlet UITextField *word5;
@property (weak, nonatomic) IBOutlet UITextField *word6;
@property (weak, nonatomic) IBOutlet UITextField *word7;

@property (strong, nonatomic) UITextField *activeTextField;

@end

@implementation MyFillerWordsTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated{
    self.word1.text = self.fillerwords[0];
    self.word2.text = self.fillerwords[1];
    self.word3.text = self.fillerwords[2];
    self.word4.text = self.fillerwords[3];
    self.word5.text = self.fillerwords[4];
    self.word6.text = self.fillerwords[5];
    self.word7.text = self.fillerwords[6];
    
}


//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/



-(void)viewWillDisappear:(BOOL)animated {
    
    NSMutableArray *words = [[NSMutableArray alloc] initWithCapacity:7];
    [words addObject:self.word1.text];
    [words addObject:self.word2.text];
    [words addObject:self.word3.text];
    [words addObject:self.word4.text];
    [words addObject:self.word5.text];
    [words addObject:self.word6.text];
    [words addObject:self.word7.text];
    
    self.fillerwords = words;
    if ([_optionsDelegateObject respondsToSelector:@selector(optionsSendingFillerWordsArray:)])
    {
        [_optionsDelegateObject optionsSendingFillerWordsArray:self.fillerwords];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.destinationViewController isKindOfClass:[ViewController class]]) {
        ViewController *vc = [segue destinationViewController];
        vc.fillerWordsArray = [self getFillerWordsFromTextFields];
    }
}
*/

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma Textfield Delegate methods

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    self.activeTextField = nil;
    
    textField.text = [textField.text uppercaseString];
}

// It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    UIScrollView *scrollView = self.tableView;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.tableView.bounds;
    aRect.size.height -= kbSize.height;
    CGRect activeRect = [self.activeTextField convertRect:self.activeTextField.frame toView:self.tableView];
    
    if (!CGRectContainsPoint(aRect, activeRect.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeRect.origin.y-kbSize.height+10);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
}

 //Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

@end
