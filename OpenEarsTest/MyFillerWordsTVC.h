//
//  MyFillerWordsTVC.h
//  FillerWordTracker
//
//  Created by  on 11/8/15.
//  Copyright © 2015 Anuradha Pai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OptionsDelegate <NSObject>

- (void) optionsSendingFillerWordsArray: (NSMutableArray *) fillerWordsArray;

@end

@interface MyFillerWordsTVC : UITableViewController

@property (nonatomic, strong) id <OptionsDelegate> optionsDelegateObject;

@property (nonatomic, strong) NSMutableArray *fillerwords;
@end
