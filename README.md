#README #

FillerWordTracker is an iOS app that can be used to detect and track filler words used in a conversation. You can play around with the app here on [Appetize.io](https://appetize.io/app/71430mjjg0atpww2d0mt17w6r8) **

### What is this repository for? ###

* FillerWordTracker can be used to detect and track filler words used in a conversation. The user has an option to give what filler words he would like to track.
* Version 1.0

### How do I get set up? ###

* Uses OpenEarsTM Framework
* Built on XCode 7.0
* Compatible with iOS 9.0 and below

### Deployment ###

* Download the project and run in XCode
* App works in the Simulator too


### Who do I talk to? ###

* Email Anuradha Pai at pai.anuradha248@gmail.com

**Note: Appetize.io cannot detect your voice. You can have a visual of the app there before downloading the project to see it running.